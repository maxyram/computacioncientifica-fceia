module kinds
implicit none
integer, parameter :: IKIND1 = SELECTED_INT_KIND(2)   ! from -128 to 127
integer, parameter :: IKIND2 = SELECTED_INT_KIND(4)   ! from -32768 to 32767
integer, parameter :: IKIND4 = SELECTED_INT_KIND(9)   ! from -2147483648 to 2147483647
integer, parameter :: IKIND8 = SELECTED_INT_KIND(18)  ! from -9223372036854775808 to 9223372036854775807
integer, parameter :: IKIND16 = SELECTED_INT_KIND(34) ! supported by gfortran but not by ifort
integer, parameter :: RKIND4=selected_real_kind(6)
integer, parameter :: RKIND8=selected_real_kind(15)
integer, parameter :: RKIND10=selected_real_kind(18)   ! for gfortran compiler (kind=10 does not exist for ifort)
integer, parameter :: RKIND16=selected_real_kind(33)   
end module kinds
