module my_funcs
        use kinds
        implicit none

        integer(ikind4), parameter :: order=2

        contains

                function deriv(x,y)
                        real(rkind8), intent(in) :: x, y(order)
                        real(rkind8)             :: deriv(order)

                        !Declaracion de variables internas
                        real(rkind8) :: b=0.0, m=1.0, k=1.0

                        deriv(1)= y(2)
                        deriv(2)= -b/m*y(2)-k/m*y(1)
                end function
end module
