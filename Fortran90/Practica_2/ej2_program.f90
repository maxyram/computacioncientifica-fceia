program ej2
        use kinds
        implicit none
        integer(ikind4) :: n_step, i
        real(rkind8) :: x_inf, x_sup, h, y0, x, y

        open(unit=10, file='ej2_datos.dat', status='old')

        read(10,*) x_inf
        read(10,*) x_sup
        read(10,*) h
        read(10,*) y0

        !write(*,*) x_inf, x_sup, h, y0
        close(10)
        
        n_step = (x_sup-x_inf)/h
        
        !archivo para guardar la solucion
        open(unit=20, file='resultado.dat', status='new')
        !seteo de condiciones iniciales
        x=x_inf
        y=y0
        write(20,*) x, y
        write(*,*) x, y
        do i=1,n_step
             call euler(x, y, h)
             write(20,*) x, y
             write(*,*) x, y
        enddo

        close(20)
end program
