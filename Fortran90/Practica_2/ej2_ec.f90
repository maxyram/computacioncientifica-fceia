function deriv(x,y)
      use kinds
      implicit none
      !declaracion de argumentos
      real(rkind8), intent(in) :: x,y
      real(rkind8)             :: deriv

      deriv = -2.0*x**3+12.0*x**2-20.0*x+8.5
end function
