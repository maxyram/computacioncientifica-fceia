program ej_cycle
      implicit none
      integer :: i

      do i=1,15
         if (mod(i,3)/=0) cycle
         print *, i
      enddo
end program
