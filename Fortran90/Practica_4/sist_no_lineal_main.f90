program ejemplo_teoria
      use nl_my_funcs
      use sistemas_no_lineales
      implicit none

      integer(4) :: itermax=100, n_iter
      real(8) :: x_i(2), x_sol(2), err, tol=1e-10

      !Solucion propuesta en el ejemplo de teoria
      !Seria mas general leerla desde archivo
      x_i = [2.0, 0.25]

      call newton_raphson(x_i, tol, itermax, x_sol, err, n_iter)

      print *, 'solucion encontrada', x_sol
      print *, 'f(x_sol)=', sistema(x_sol)

end program
