module sistemas_lineales
      implicit none

      contains

      subroutine gauss_jordan(A, b, x)
              ! A.x=b
              implicit none
              !Declaracion de argumentos
              real(8), intent(in)  :: A(:,:), b(:)       !<- arreglos automaticos
              real(8), intent(out) :: x(size(b))

              !Declaracion de variables internas
              integer(4) :: i,j, n
              real(8) :: factor
              real(8) :: Aamp(size(A,1), size(A,2)+1)    ! Aamp=[A|b]

              n=size(A,1)
              !Generacion de la matriz ampliada
              Aamp(1:n,1:n)=A
              Aamp(:,n+1)=b

              !print *, 'Imprimo la matriz ampliada'
              !do i=1,n
              !   print *, Aamp(i,:)
              !enddo
              
              !Proceso de Gauss-Jordan
              do i=1,n
                if (Aamp(i,i)==0.0) then
                        print *, 'Elemento pivote igual a cero'
                        print *, 'Use una subrutina con pivoteo'
                        stop
                      endif

                 do j=1,n
                   if(j==i) cycle
                   factor=Aamp(j,i)/Aamp(i,i)
                   Aamp(j,:)=Aamp(j,:)-factor*Aamp(i,:)
                 enddo
                 Aamp(i,:)=Aamp(i,:)/Aamp(i,i)
              enddo


              !Ponemos la solucion en x
              x=Aamp(:,n+1)
      end subroutine

      subroutine gauss_jordan_inv(A, Ainv)
              ! Calculo de matriz inversa usando Gauss Jordan
              implicit none
              !Declaracion de argumentos
              real(8), intent(in)  :: A(:,:)       !<- arreglos automaticos
              real(8), intent(out) :: Ainv(size(A,1), size(A,2))

              !Declaracion de variables internas
              integer(4) :: i,j, n
              real(8) :: factor
              real(8) :: Aamp(size(A,1), 2*size(A,2))    ! Aamp=[A|I]

              n=size(A,1)
              !Generacion de la matriz superampliada
              Aamp(1:n,1:n)=A
              !Aca se genera la identidad de la parte derecha de la matriz superampliada
              Aamp(:,n+1:2*n)=0.0_8
              do i=1,n
                 Aamp(i,n+i)=1.0_8
              enddo

              !print *, 'Imprimo la matriz ampliada'
              !do i=1,n
              !   print *, Aamp(i,:)
              !enddo

              !Proceso de Gauss-Jordan
              do i=1,n
                if (Aamp(i,i)==0.0) then
                        print *, 'Elemento pivote igual a cero'
                        print *, 'Use una subrutina con pivoteo'
                        stop
                      endif

                 do j=1,n
                   if(j==i) cycle
                   factor=Aamp(j,i)/Aamp(i,i)
                   Aamp(j,:)=Aamp(j,:)-factor*Aamp(i,:)
                 enddo
                 Aamp(i,:)=Aamp(i,:)/Aamp(i,i)
              enddo


              !Ponemos la solucion en Ainv
              Ainv=Aamp(:, n+1:2*n)


      end subroutine
end module

!Ejemplo de uso de funcion SIZE
!
!supongamos A(5,5)
!            size(A) : da 25
!            size(A,1) : da 5
!            size(A,2) : da 5

!supongamos A(5,7)
!            size(A) : da 35
!            size(A,1) : da 5
!            size(A,2) : da 7
