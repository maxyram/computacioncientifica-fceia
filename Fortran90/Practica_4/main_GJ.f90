program GJ_ejemplo
      use sistemas_lineales
      implicit none
      integer(4) :: n,i,j
      real(8), allocatable :: M(:,:), b(:), sol(:), M_inv(:,:)

      open(unit=10, file='datos_GJ.dat', status='old')
      read(10,*) n
      allocate(M(n,n), b(n), sol(n), M_inv(n,n))
      do i=1,n
        read(10,*) M(i,:)
      enddo
      read(10,*) b
      close(10)

      call gauss_jordan(M, b, sol)

      write(*,*) 'Solucion hallada'
      write(*,*) sol
      write(*,*) 'Chequeo de la solucion: A.sol-b=0'
      write(*,*) matmul(M,sol)-b

      write(*,*)' '
      write(*,*)'=========================='
      write(*,*)'Ahora hallamos la inversa de M'
      call gauss_jordan_inv(M, M_inv)
      write(*,*) 'Matriz inversa:'
      do i=1,n
        write(*,*) M_inv(i,:)
      enddo
end program
