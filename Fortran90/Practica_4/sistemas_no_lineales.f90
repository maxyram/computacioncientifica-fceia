module sistemas_no_lineales
        use sistemas_lineales
        use nl_my_funcs
        implicit none

        contains

        subroutine newton_raphson(x_ini, tol, iter_max, x_sol, err, n_iter)
               !Declaración de argumentos
               integer(4), intent(in)  :: iter_max
               real(8), intent(in)     :: tol, x_ini(:)
               integer(4), intent(out) :: n_iter
               real(8), intent(out)    :: err, x_sol(size(x_ini))

               !Declaracion de variables internas
               integer(4) :: iter=1
               !real(8) :: f(size(x_ini)), jac(size(x_ini), size(x_ini))
               real(8) :: x(size(x_ini)), dx(size(x_ini))

               x=x_ini
               do
                  !f=sistema(x)
                  !jac=jacobiana(x)

                  !call gauss_jordan(jac, -f, dx)

                  call gauss_jordan(jacobiana(x), -1.0*sistema(x), dx)

                  x=x+dx
                  err=norm2(dx)
                  if (err < tol) then
                          x_sol=x
                          n_iter=iter
                          return
                  endif

                  if (iter == iter_max) then
                        write(*,*) 'El sistema no converge en', iter_max, 'iteraciones'
                        stop
                  endif
                  iter=iter+1 
               enddo


        end subroutine

end module
