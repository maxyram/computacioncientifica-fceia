module nl_my_funcs
      implicit none

      contains

      function sistema(x)
              real(8), intent(in) :: x(:)
              real(8)             :: sistema(size(x))

              sistema(1) = x(1)**2-2.0*x(1)-x(2)+0.5
              sistema(2) = x(1)**2+4.0*x(2)**2-4.0
      end function

      function jacobiana(x)
              real(8), intent(in) :: x(:)
              real(8)             :: jacobiana(size(x), size(x))

              jacobiana(1,1)=2.0*x(1)-2.0
              jacobiana(1,2)=-1.0
              jacobiana(2,1)=2.0*x(1)
              jacobiana(2,2)=8.0*x(2)
      end function
end module
