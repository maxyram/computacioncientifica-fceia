module interpolacion
    implicit none

    contains
    
    real(8) function pol_lagrange(x_dat, y_dat, x)
        !declaracion de argumentos
        real(8), intent(in) :: x_dat(:), y_dat(:), x
        !declaracion de variables internas
        integer(4) :: k
        real(8)    :: suma
        suma = 0._8

        do k=1,size(x_dat)
            suma=suma+y_dat(k)*A_k(x_dat, x, k)/A_k(x_dat, x_dat(k), k)
        enddo

        pol_lagrange=suma

    end function

    real(8) function A_k(x_dat, x, k)
        !declaracion de argumentos
        integer(4), intent(in) :: k
        real(8), intent(in)    :: x_dat(:), x
        !declaracion de variables internas
        integer(4) :: j
        real(8)    :: prod

        prod=1._8
        do j=1,size(x_dat)
            if (j==k) cycle
            prod=prod*(x-x_dat(j))
        enddo

        A_k=prod
    end function

end module
