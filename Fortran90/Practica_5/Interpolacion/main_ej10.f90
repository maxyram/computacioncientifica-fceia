program ej10
    use interpolacion
    implicit none
    integer(4)           :: n_dat, i, n_interp=101
    real(8)              :: h, x, y 
    real(8), allocatable :: x_dat(:), y_dat(:)
    
    
    open(unit=5, file='p5_ej10.dat')
            read(5,*) n_dat
            allocate(x_dat(n_dat), y_dat(n_dat))
            do i=1,n_dat
                    read(5,*) x_dat(i), y_dat(i)
                    write(*,*) x_dat(i), y_dat(i)
            enddo
    close(unit=5)
    
    !hacer una rutina que ordene los datos x_dat e y_dat de forma creciente
    
    h=(maxval(x_dat)-minval(x_dat))/(n_interp-1)

    open(unit=20, file='resultados_ej10.dat', status='unknown')
    do i=1,n_interp
        x=minval(x_dat)+(i-1)*h

        y=pol_lagrange(x_dat, y_dat, x)

        write(20,*) x, y

    enddo

    close(20)
    deallocate(x_dat, y_dat)
    
    
    end program
