module my_funcs
      implicit none

      contains
      function f(x, n_param)
              integer(4), intent(in) :: n_param
              real(8), intent(in)    :: x
              real(8)                :: f(n_param)

              f(1) = 1._8
              f(2) = x
              f(3) = x*x
              f(4) = x*x*x
      end function


end module

