module ajuste
      use my_funcs
      use sistemas_lineales
      implicit none

      contains

      subroutine min_cuad_lineales(x, y, a, err)
              !declaracion de argumentos
              real(8), intent(in)  :: x(:), y(:) !assumed shape arrays
              real(8), intent(out) :: a(:), err

              !declaracion de variables internas
              integer(4) :: i, n_dat, n_param
              real(8) :: z(size(x),size(a)), zt(size(a), size(x)), ztz(size(a), size(a))
              real(8) :: ztz_inv(size(a), size(a)), zty(size(a))
              

              n_dat=size(x)
              n_param=size(a)

              do i=1,n_dat
                  z(i,:)=f(x(i), n_param)
                  !print *, z(i,:)
              enddo

              zt=transpose(z)

              ztz=matmul(zt,z)

              call gauss_jordan_inv(ztz, ztz_inv)

              zty=matmul(zt, y)

              a=matmul(ztz_inv, zty)
              err=norm2(y-matmul(z,a))
              

      end subroutine


end module
