program ejemplo_teoria
      use ajuste
      implicit none
      integer(4)           :: n_dat, n_param
      integer(4)           :: i
      real(8), allocatable :: x(:), y(:), a(:)
      real(8)              :: error


      open(unit=10, file='data_linear.in', status='old')
      read(10,*) n_dat

      !alojo la memoria
      allocate(x(n_dat), y(n_dat))
      !leo los datos a ajustar
      do i=1,n_dat
        read(10,*) x(i), y(i)
      enddo
      read(10,*) n_param
      allocate(a(n_param))
      close(10)

      !Escritura de datos para chequear la correcta lectura
      !write(*,*) 'ndat=', n_dat
      !write(*,*) 'nparam=', n_param
      !do i=1,n_dat
      !  write(*,*) x(i), y(i)
      !enddo

      call min_cuad_lineales(x, y, a, error)

      print *, 'Los parametro obtenidos son:'
      print *, a

      print *, 'Error de ajuste:', error


      deallocate(x, y, a)
end program
