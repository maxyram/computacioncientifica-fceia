!Este codigo debe compilarse poniendo primero el archivo con el modulo
! 'constantes', segundo el archivo con el modulo 'arreglos' y despues
! los demas archivos, por ejemplo:
!
! gfortran ejemplo2_mod_a.f90 ejemplo2_mod_b.f90 ejemplo2_prog.f90 ejemplo2_sub.f90 -ejemplo2.x
!
! Si los archivos .f90 se ponen en otro orden va a dar un error diciendo que no existe
! el modulo dimensiones
program ejemplo2
    use arreglos
    implicit none
    integer(4) :: i=5

    write(*,*) 'Tamaño del arreglo a:', size(a)
    write(*,*) ' ' 
    write(*,*) 'Tamaño del arreglo b:', size(b)
    write(*,*) ' ' 
    write(*,*) 'Forma del arreglo b:', shape(b)
    write(*,*) ' ' 

    write(*,*) 'Escribo el arreglo a desde el programa antes de modificarlo'
    write(*,*) a

    ! Ver que el arreglo a no es argumento de la subrutina
    ! sin embargo la subrutina puede acceder al mismo a traves del
    ! modulo arreglo.
    ! La subrutina y el programa principal no pueden acceder al valor de 
    ! ndim porque no tienen acceso al modulo 'constantes'
    call sub_ejemplo2(i)
    write(*,*) 'Escribo el arreglo a desde el programa despues de modificarlo'
    write(*,*) a

end program