! En este modulo se declara un parametro entero que se usara
! para dimensionar arreglos
module dimensiones
    implicit none
    integer, parameter :: ndim = 4
end module