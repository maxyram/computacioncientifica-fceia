! En el codigo se define una funcion (que no tiene mucho sentido)
! y se llama a la misma desde el programa pricipal. 
! Tanto el programa como la funcion hacen uso de la variable pi,
! que no esta definida en ninguno de los dos.
! La variable 'pi' se define en el modulo constantes, y ese valor
! se hace disponible al programa y a la funcion mediante la sentencia:
! use constantes
!
! En este caso en el modulo se ha definido un parametro, cuyo valor no se
! puede modificar. En otro ejemplo veremos el caso de variables cuyo valor 
! si puede ser modificado.
!
! ATENCION
! En la compilacion los archivos que tienen los modulos deben ir primero
!
! gfortran -o ejemplo1.x ejemplo1_mod.f90 ejemplo1_prog.f90 
!
! En caso de que se escriba todo en un único archivo, el modulo debe estar
! el principio del archivo.
!
program ejemplo1_modulo
    use constantes           ! los USE van antes del IMPLICIT NONE
    implicit NONE

    integer, parameter :: np=10
    integer :: i
    real(8) :: fun_sin

    do i=0,np
        write(*,*) i, i*pi/np, fun_sin(i, np)   ! El programa usa pi
    enddo

end program

real(8) function fun_sin(i, n)
    use constantes
    implicit none
    !Declaracion de argumentos
    integer, intent(in) :: i, n

    fun_sin = sin(i*pi/n)                        ! La funcion usa pi
end function