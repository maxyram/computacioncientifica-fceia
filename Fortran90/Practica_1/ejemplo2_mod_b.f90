! Este modulo usa la variable el modulo 'dimensiones'
! Se hace en modulos separado solo para ilustrar que es importante
! el orden de compilacion de los modulos
!
! Como el module dimensiones es usado por el modulo arreglos
! el archivo que contiene al modulo dimensiones debe estar primero
! en la linea de compilacion
module arreglos
    use dimensiones
    implicit none

    real(8), dimension(ndim) :: a
    real(4)                  :: b(ndim,ndim)
end module