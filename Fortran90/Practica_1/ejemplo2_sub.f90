subroutine sub_ejemplo2(i)
    use arreglos
    implicit none
    integer, intent(in) :: i
    integer :: j
    
    write(*,*) 'Escribo el arreglo a desde la subrutina antes de modificarlo'
    write(*,*) a
    do j=1,size(a)
        a(j)=i+j
    enddo

    write(*,*) 'Escribo el arreglo a desde la subrutina despues de modificarlo'
    write(*,*) a
end subroutine
