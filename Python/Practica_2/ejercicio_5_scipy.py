from integradores import *
import numpy as np
import matplotlib.pyplot as plt

def pendulo_simple(x, y):
    g = 9.8
    L = 0.5
    dy = np.zeros_like(y)
    dy[0] = y[1]
    dy[1] = -g/L*y[0] 
    return dy

# Condiciones iniciales
x_ini = 0.0
x_fin = 2*np.pi
h = 0.05
y_ini = 0.2
yp_ini = 0.0

# number of points
n = int((x_fin-x_ini)/h+1)

x_s = np.linspace(x_ini, x_fin, num=n)
sol = np.empty((n,2))
sol[0,:] = [y_ini, yp_ini]
#x_aux = x_ini
#y_aux = [y_ini, yp_ini]

for i, (x,y) in enumerate(zip(x_s, sol)):
    if i==n-1: break
    x_s[i+1], sol[i+1,:] = rk4(x, y, h, pendulo_simple)

print(x_s)
plt.plot(x_s, sol[:,0])
plt.plot(x_s, sol[:,1])

plt.show()