from ejercicio_1 import *
import numpy as np
import matplotlib.pyplot as plt

def ejemplo_teoria(x, y):
    dy = -2*x**3+12*x**2-20*x+8.5
    return dy
    
h = 0.1


x = 0.0
y = 1.0
x_s = [x]
y_euler = [y]
y_heun = [y]
y_pm = [y]
y_rk4 = [y]


while x < 4:
    x, y = euler(x, y, h, deriv=ejemplo_teoria)
    x_s.append(x)
    y_euler.append(y)

x = 0.0; y = 1.0
while x < 4:
    x, y = heun(x, y, h, deriv=ejemplo_teoria)
    y_heun.append(y)


x = 0.0; y = 1.0
while x <=4:
    x, y = punto_medio(x, y, h, deriv=ejemplo_teoria)
    y_pm.append(y)

x = 0.0; y = 1.0
while x < 4:
    x, y = rk4(x, y, h, deriv=ejemplo_teoria)
    y_rk4.append(y)


plt.plot(x_s, y_euler, label='Euler')
plt.plot(x_s, y_heun, label='Heun')
plt.plot(x_s, y_pm, label='pto medio')
plt.plot(x_s, y_rk4, label='RK4')
plt.legend()
plt.xlabel('x')
plt.ylabel('y')
plt.text(2.0, 5.0, 'h=0.10', fontsize=14)
plt.show()
#plt.savefig('ejercicio_1.png', format='png')