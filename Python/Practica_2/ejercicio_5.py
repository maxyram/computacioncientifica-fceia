from integradores import *
from scipy.integrate import solve_ivp
import numpy as np
import matplotlib.pyplot as plt

##################################################
# escribo la funcion deriv que define el problema
def pendulo_simple(x, y):
    g = 9.8
    L = 0.5
    dy = np.empty_like(y)
    dy[0] = y[1]
    dy[1] = -g/L*y[0] 
    return dy


def pendulo(x, y):
    g = 9.8
    L = 0.5
    dy = np.empty_like(y)
    dy[0] = y[1]
    dy[1] = -g/L*np.sin(y[0]) 
    return dy

##################################################
# Condiciones iniciales
# (esto se podria leer de un archivo)
x_ini = 0.0
x_fin = 2*np.pi
h = 0.01
theta_ini = np.pi-0.01
omega_ini = 0.0

##################################################
# Quiero almacenar la solucion en un arreglo en 
# lugar de usar una lista. Asi que tengo que calcular
# el tamaño del arreglo.

# numero de puntos en el arreglo de abcisas
n = int((x_fin-x_ini)/h+1)

# creo el arreglo con los valores de la var ind
x_s = np.linspace(x_ini, x_fin, num=n)

################################################################
# Solucion con metodo propio
sol = np.empty((2, n))
sol[:, 0] = [theta_ini, omega_ini]

for i in range(n-1):
    x_s[i+1], sol[:, i+1] = rk4(x_s[i], sol[:,i], h, pendulo_simple)

# Sin aprox de angulo pequeño
sol2 = np.empty((2, n))
sol2[:, 0] = [theta_ini, omega_ini]

for i in range(n-1):
    x_s[i+1], sol2[:, i+1] = rk4(x_s[i], sol2[:,i], h, pendulo)


################################################################
# Solucion con SciPy

scp = solve_ivp(fun=pendulo, t_span=(x_ini, x_fin), method='RK45', 
                y0=[theta_ini, omega_ini], t_eval=x_s, max_step=h)

## def periodo(x, y):
##     return y[0]
## periodo.direction = -1
## 
## scp = solve_ivp(fun=pendulo_simple, t_span=(x_ini, x_fin), 
##                 y0=[y_ini, yp_ini], method='RK45', events=periodo)
## 
## print(type(scp.t_events))
## print(type(scp.t_events[0]))
## ts = scp.t_events[0]
## ys = np.vstack(scp.y_events)

print(scp.y[0])



################################################################
fig, ax = plt.subplots(2)
ax[0].plot(x_s, sol[0,:], 'ro', label='RK4 - peq osc')
ax[0].plot(x_s, sol2[0,:], 'g^', label='RK4 - sin aprox')
ax[0].plot(scp.t, scp.y[0], 'b*', label='SciPy')
ax[0].set_ylabel('angulo (rad)')
## ax[0].plot(ts, ys[:,0], c='greenyellow', linestyle='', marker='s', markersize=12 )

ax[1].plot(x_s, sol[1,:],'ro')
ax[1].plot(x_s, sol2[1,:], 'g^')
ax[1].plot(scp.t, scp.y[1], 'b*')
ax[1].set_xlabel('tiempo (s)')
ax[1].set_ylabel('velocidad angular (rad/s)')
## ax[1].plot(ts, ys[:,1], c='greenyellow', linestyle='', marker='s', markersize=12 )

ax[0].legend()

plt.show()