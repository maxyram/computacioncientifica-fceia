def euler(x, y, h, deriv):
    '''metodo de Euler para EDO de 1er orden'''
    k1 = deriv(x, y)
    y = y + k1*h
    x = x + h
    return x, y


def heun(x, y, h, deriv):
    '''metodo de Heun para EDO de 1er orden'''
    k1 = deriv(x, y)
    k2 = deriv(x+h, y+h*k1)
    y = y + (k1+k2)*h/2.0
    x = x + h
    return x, y


def punto_medio(x, y, h, deriv):
    '''metodo de Punto Medio para EDO de 1er orden'''
    k1 = deriv(x, y)
    k2 = deriv(x+h/2.0, y+h/2.0*k1)
    y = y + k2*h
    x = x + h
    return x, y


def rk4(x, y, h, deriv):
    '''metodo RK4 para EDO de 1er orden'''
    k1 = deriv(x, y)
    k2 = deriv(x+h/2.0, y+h/2.0*k1)
    k3 = deriv(x+h/2.0, y+h/2.0*k2)
    k4 = deriv(x+h, y+h*k3)
    y = y + (k1+2.0*(k2+k3)+k4)/6.0*h
    x = x + h 
    return x, y
