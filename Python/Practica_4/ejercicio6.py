from scipy.optimize import root
import numpy as np

def fun6(x):
    '''
    Sistema no lineal del ejercicio 6
    '''
    f = np.empty_like(x)

    f[0] = x[0]**2+x[0]+x[1]**2-x[1]+x[2]**3-3.0*x[2]**2-13.0*x[2]+9.0
    f[1] = x[0]**2-2.0*x[0]+x[1]**2+x[2]-6.0
    f[2] = x[0]**3-2.0*x[0]**2+x[1]+x[2]**3-3.0*x[2]**2-25*x[2]+74.0

    return f

def jac6(x):
    '''
    Jacobiana del sistema del ejercicio 6
    '''
    jac = [[2.0*x[0]+1.0        , 2.0*x[1]-1.0, 3.0*x[2]**2-6.0*x[2]-13.0 ],
           [2.0*x[0]-2.0        , 2.0*x[1]    , 1.0                       ],
           [3.0*x[0]**2-4.0*x[0], 1.0         , 3.0*x[2]**2-6.0*x[2]-25.0 ]]
    return jac

################################################
# Solucion inicial
x0 = np.array([2.8, 3.9, 5.5])


print('Solucion utilizando la funcion root de SciPy')

sol = root(fun6, x0)
print('sol es un objeto que tiene la solucion, informacion de la convergencia y otras cosas.\npara acceder al vector solucion se debe invocar sol.x')
print('\nvector solucion: {}'.format(sol.x))
print('fun(sol)={}'.format(fun6(sol.x)))
print('fun(sol)={}'.format(sol.fun))
