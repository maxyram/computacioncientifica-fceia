import numpy as np
from sys import exit

def GaussJordan(mat, b):
    '''Metodo de Gauss Jordan para resolucion de sistemas 
    de ecuaciones algebraicas lineales: mat*x = b'''

    # Creacion de la matriz ampliada
    amp = np.empty((mat.shape[0], mat.shape[1]+1))
    
    # Armado de la matriz ampliada
    amp[:, :-1] = mat
    amp[:,-1] = b

    # Proceso de Gauss Jordan
    for i in range(amp.shape[0]):
        # pivoteo parcial
        row_max_abs_elem = np.argmax(np.abs(amp[:,i]))   # determina la fila en que esta el maximo
        if (i<row_max_abs_elem):                         # intercambia solo si el elemento mayor esta por debajo de la fila pivote
            amp[i,:], amp[row_max_abs_elem,:] = amp[row_max_abs_elem,:].copy(), amp[i,:].copy() # intercambio de filas
        
        # reduccion de filas
        for r in range(amp.shape[0]):
            if i==r: continue                            # salteo la fila pivote
            fact = amp[r,i]/amp[i,i]
            amp[r,:] = amp[r,:]-fact*amp[i,:]

    return amp[:,-1]/amp.diagonal()
        
    
def jacobi(mat, b, x0=None, tol=1e-9, maxiter=100):
    '''
    Metodo de Jacobi para sistemas de ecuaciones lineales.
    Args:
      mat:      matriz de coeficientes
      b:        vector de terminos independientes
      x0:       solucion inicial (opcional)
      tol:      toleracia de error para convergencia (default: 1e-9)
      maxiter:  numero maximo de iteraciones (default: 100)
    '''

    # Armado de la matriz auxiliar (con los elementos diagonales iguales a cero)
    aux = mat-np.diag(np.diag(mat))

    if x0==None: x0=np.random.rand(b.size)  # Si no se pasa semilla, se genera una aleatoria

    for iter in range(maxiter):
        x = (b-np.matmul(aux,x0))/np.diagonal(mat)
        if np.linalg.norm(x-x0)<tol:
            return x
        x0=x
    else:
        print('El metodo no convergio luego de {} iteraciones'.format(maxiter))
        exit()


