import os
import numpy as np
from sistemas_lineales import GaussJordan, jacobi

# Defino la ruta al archivo de datos
carpeta_actual = os.getcwd()
archivo_matriz = 'matriz_ej3_diagdom.dat'
ruta_archivo = os.path.join(carpeta_actual, archivo_matriz)

# Lectura del archivo de datos
matA = np.loadtxt(ruta_archivo, max_rows=5)
b = np.loadtxt(ruta_archivo, skiprows=5)

print('matriz: \n {}'.format(matA))
print('vector independiente:\n{}'.format(b))

print('====================\nMetodo de Gauss-Jordan')
x = GaussJordan(matA, b)

print('\nvector solucion:\n{}'.format(x))

print('\n\nVerificacion de la solucion: ¿A.x==b?')

Ax = np.matmul(matA, x)
print('A.x=\n{}'.format(Ax))
print('b=\n{}'.format(b))

print('\nNorma(Ax-b)={}'.format(np.linalg.norm(Ax-b)))

print('\nOtra forma de chequear')
print('La funcion np.isclose compara componenete a componente y devuelve True si la diferencia es menor a rtol')
print('\n¿Ax y b son muy parecidos?:{}'.format(np.isclose(Ax, b, rtol=1e-12)))

print('====================\nMetodo de Jacobi:')
x = jacobi(matA,b, maxiter=20)
print('\nvector solucion:\n{}'.format(x))

print('\n\nVerificacion de la solucion: ¿A.x==b?')

Ax = np.matmul(matA, x)
print('A.x=\n{}'.format(Ax))
print('b=\n{}'.format(b))

print('\nNorma(Ax-b)={}'.format(np.linalg.norm(Ax-b)))