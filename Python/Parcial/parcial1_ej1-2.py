import numpy as np
import matplotlib.pyplot as plt

# leer archivo
datos = np.loadtxt('resultados_funcion.dat')

print(datos.shape)

#fig = plt.figure()
for i in range(1,datos.shape[1]):
    str_leg = 'sen(' + str(2*i) + '*x)'
    plt.plot(datos[:,0], datos[:,i], label=str_leg)
plt.xlabel('x')
plt.ylabel('y')
plt.legend()
#plt.show()
plt.savefig('grafico.pdf')
