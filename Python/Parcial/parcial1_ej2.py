import numpy as np
import sys 

def contar_ceros_fila(mat):
    '''
    Determinar la cantidad de ceros en cada 
    fila de la matriz mat
    '''
    count = np.zeros(np.shape(mat)[0], dtype='int') # arreglo con igual cant de filas que mat
    for i in range(np.shape(mat)[0]):
        for j in range(np.shape(mat)[1]):
            if mat[i,j] == 0:
                count[i] += 1
    return count

def elegir_fila(arr):
    '''
    Elegir la fila con mas cantidad de ceros. 
    En caso de haber varias con la maxima 
    cantidad, elegir la primera de ellas'''
    mayor = np.max(arr)
    for i in range(arr.size):
        if arr[i]==mayor:
            return i

def determinante(mat, fila):
    '''
    Calculo del determinante de la matriz mat
    desarrollado por la fila fila
    '''
    dim = np.shape(mat)[0]
    if dim == 2:
        det = mat[0,0]*mat[1,1]-mat[1,0]*mat[0,1]
    elif dim < 2:
        print('ERROR: matriz de dimension < 2')
        sys.exit()
    else:
        fila = fila
        det = 0
        for col in range(dim):
            aux = np.delete(mat,fila,0)
            adj = np.delete(aux,col,1)
            det += mat[fila,col]*(-1)**(fila+col)*determinante(adj, fila)
    return det


#################################################################################
#
# leer la matriz
with open('matriz.dat', 'r') as f:
    matriz = np.loadtxt(f, skiprows=1)

print(matriz)

# determinar cantidad de zeros en cada fila
zeros_fila = contar_ceros_fila(matriz)
print(zeros_fila)

# elegir fila con mas ceros para desarrollar el determinante
fila = elegir_fila(zeros_fila)

# calculo del determinante
det = determinante(matriz, fila)

print('determinante = {}'.format(det))

det_np = np.linalg.det(matriz)

print('determinante Numpy = {}'.format(det_np))
