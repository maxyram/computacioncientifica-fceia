import numpy as np
from numpy import pi

def f(x, n):
    return np.sin(x*n)


#################################################################

x = np.linspace(-pi, pi, num=101, endpoint=True)

# print(x)

# Defino un arreglo de 6 columnas y la misma cantidad de filas que x
resultados = np.empty((x.size, 6))

# Almaceno lo valores de x en la columna 0 del arreglo
resultados[:,0] = x

# Calculo la funcion en todos los x y lo guardo en cada columna del arreglo resultados
for n in range(1,6):
    resultados[:,n] = f(x, 2*n)

#print(resultados)

# Escribo el arreglo resultado en un archivo
np.savetxt('resultados_funcion.dat', resultados)