import sys
from numpy import sign

def bisec(fun, x_lo, x_hi, tol=1e-9):
    if x_lo > x_hi:
        print('Error, el intervalo no es valido')
        print('x_lo={} debe ser menor a x_hi={}'.format(x_lo, x_hi))
        sys.exit()
    if sign(fun(x_lo))*sign(fun(x_hi)) > 0:
        print('La funcion tiene el mismo signo en ambos extremos del intervalo \nfun(x_lo)={} \nfun(x_hi)={})'.format(fun(x_lo), fun(x_hi)))
        sys.exit()
    x_r = (x_hi+x_lo)/2.
    err = (x_hi-x_lo)/2.
    #print(x_r, err, fun(x_r))
    if (err<tol and fun(x_r)<tol):
        return x_r, err
    elif (sign(fun(x_r))== sign(fun(x_lo))):
        x_r, err = bisec(fun, x_r, x_hi, tol)
        return x_r, err
    else:
        x_r, err = bisec(fun, x_lo, x_r, tol)
        return x_r, err
    

def newton(fun, dfun, x0, tol=1e-9):

