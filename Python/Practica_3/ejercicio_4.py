from metodos_raices import bisec
from scipy import optimize


def fun_ej4(x):
    return x**3-x-1.0


raiz, error = bisec(fun_ej4, 1., 2., 1e-12)
 
print('La raiz es: raiz = {}'.format(raiz))
print('La funcion vale: f(raiz)={}'.format(fun_ej4(raiz)))
print('El error es: error={}'.format(error))

###################################################################
# Implementacion con scipy

print('\nSolucion con SciPy')

raiz, info = optimize.bisect(fun_ej4, 1., 2., full_output=True)
print('La raiz es: raiz = {}'.format(raiz))
print('La funcion vale: f(raiz)={}'.format(fun_ej4(raiz)))
print('Informacion de convergencia:\n{}'.format(info))