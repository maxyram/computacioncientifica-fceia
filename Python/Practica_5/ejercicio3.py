import numpy as np
import matplotlib.pyplot as plt
from ajuste_lineal import min_cuad_lin

def F(x):
    '''
    funcion de ajuste para el problema 3
    como se quiere ajustar con un polinomio de la forma:
    c1 + c2 x + c3 x^2
    la funcion F se define como:
    F(x) = [f1(x), f2(x), f3(x)]
    con:
    f1(x)=1
    f2(x)=x
    f3(x)=x**2
    '''

    #return np.array([1.0, np.cos(x), np.log(x) ])
    return np.array([1.0, x, x**2])

filename = 'p5_ej03.dat'

# Leo archivo de datos
x_dat, y_dat = np.loadtxt(filename, dtype='float', skiprows=1, unpack=True)


# Calculo de parametros de ajuste
p, err = min_cuad_lin(fun=F, x=x_dat, y=y_dat )
print('\nLos parametros de ajuste son:\n{}'.format(p))
print('El error cuadratico medio es: \nRMSE={}'.format(err))

# Determinacion de minimo y maximo en x_dat
x_min, x_max = np.min(x_dat), np.max(x_dat) 
# Generacion de puntos x para la funcion ajustada
x_fit = np.linspace(x_min, x_max, num=100)
# Generacion de puntos con la funcion ajustada
y_fit = [np.dot(F(x), p) for x in x_fit]

# Grafica de datos y resultados
plt.scatter(x_dat, y_dat, color='r')
plt.plot(x_fit, y_fit, c='b')
plt.show()