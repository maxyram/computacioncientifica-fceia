import numpy as np
from numpy.linalg import inv

def min_cuad_lin(fun, x, y):
    '''
    funcion para ajustar por minimos cuadrados
    con una funcion lineal en los parametros
    '''
    n_dat = x.size
    n_param = fun(x[0]).size
    z = np.empty((n_dat, n_param))
    
    # calculo de la matriz z
    for i in range(n_dat):
        z[i,:] = fun(x[i])

    # transpuesta de matriz z
    zT = z.T

    # prod matricial z transpuesta por z
    zTz = zT@z

    # inversa de zTz
    zTz_inv = inv(zTz)

    # producto interto zT.y
    zTy = np.dot(zT,y)

    # calculo de los parametros de ajuste a
    a = zTz_inv @ zTy

    # calculo de la raiz del error cuadratico medio
    rmse = 0.0
    for i in range(n_dat):
        rmse += (y[i]-np.dot(fun(x[i]),a))**2

    rmse = np.sqrt(rmse/n_dat)
    
    return a, rmse
    
    