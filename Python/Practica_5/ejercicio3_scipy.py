import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def func(x, c1, c2, c3):
    '''
    funcion para ajustar el ejercicio 3 usando la funcion
    curve_fit de SciPy
    '''

    #return np.array([1.0, np.cos(x), np.log(x) ])
    return c1 + c2*x + c3*x**2

filename = 'p5_ej03.dat'

# Leo archivo de datos
x_dat, y_dat = np.loadtxt(filename, dtype='float', skiprows=1, unpack=True)


# Calculo de parametros de ajuste
popt, pcov = curve_fit(func, x_dat, y_dat)
print('\nLos parametros de ajuste son:\n{}'.format(popt))
#print('El error cuadratico medio es: \nRMSE={}'.format(err))

# Determinacion de minimo y maximo en x_dat
x_min, x_max = np.min(x_dat), np.max(x_dat) 
# Generacion de puntos x para la funcion ajustada
x_fit = np.linspace(x_min, x_max, num=100)
# Generacion de puntos con la funcion ajustada
y_fit = [func(x, popt[0], popt[1], popt[2]) for x in x_fit]

# Grafica de datos y resultados
plt.scatter(x_dat, y_dat, color='r')
plt.plot(x_fit, y_fit, c='b')
plt.show()