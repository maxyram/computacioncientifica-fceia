def unicos(data):
    '''Determina los valores unicos de una lista.
       Devuelve una lista con los valores unicos ordenados de menor
       a mayor
    '''
    uni = []
    for i in range(len(data)):
        if data[i] not in uni:
            uni.append(data[i])
    ordenar(uni)
    return uni


def ordenar(lista):
    '''Ordena de menor a mayor los elementos de una lista de numeros
    '''
    for i in range(len(lista)-1):
        for j in range(i+1, len(lista)):
            if (lista[j] < lista[i]):
                lista[i], lista[j] = lista[j], lista[i]
    return lista            


def contarUnicos(datos, unicos):
    '''Cuenta la cantidad de veces que aparece cada elemento del arreglo
       "unicos" dentro del arreglo "datos"
    '''
    

fichero = open('ej11.dat', 'r')
texto = fichero.readlines()

# tomo el numero de datos de la primera linea
l1 = texto[0].split()
n = int(l1[0])

datos = []
for i in range(1,n):
    datos.append(int(texto[i]))

elem_unicos = unicos(datos)
print(elem_unicos)


