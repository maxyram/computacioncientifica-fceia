def propagar(fosforos):    
    for i in range(len(fosforos)):
        if fosforos[i] == 1:
            for j in range(i+1, len(fosforos)):
                if fosforos[j]==-1: break
                if fosforos[j]==0: fosforos[j] = 1
            for j in range(i-1, -1, -1):
                if fosforos[j]==-1: break
                if fosforos[j]==0: fosforos[j] = 1
    return fosforos


fosforos = [0, 0, 1, 0, -1, 0, 0, 0, 1, 0, -1, 0 ,0, 0]

print('Inicio: {}'.format(fosforos))
fosforos = propagar(fosforos)
print('Final: {}'.format(fosforos))
