import numpy as np
import random
import matplotlib.pyplot as plt
#
def ordenar(a):
    arr = a.copy()
    for i in range(len(arr)-1):
        for j in range(i+1, len(arr)):
            if (arr[j, 0] < arr[i, 0]):
                #arr[[i, j]] = arr[[j, i]]
                aux = np.copy(arr[i,:])  # arr[i,:].copy()
                arr[i,:] = arr[j,:]
                arr[j,:] = aux
    return arr

def func(x):
    return np.sin(x**2)
#
#
x = np.empty((201, 2)) # en fortran es =>    real(8) :: x(201,2)

for i in range(len(x)):
    x[i, 0] = random.uniform(0, 2*np.pi)

x[:,1] = func(x[:,0])

x_ord = ordenar(x)

fig, ax = plt.subplots(2, sharex=True)

ax[0].plot(x[:,0], x[:,1], color='y', linestyle='--', marker='o', mfc='r')
ax[1].plot(x_ord[:,0], x_ord[:,1], color='b', marker='<')

plt.show()
