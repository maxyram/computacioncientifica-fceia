import numpy as np

# inciso a
def f(x):
    return np.sin(2.0*x)

# inciso b
x1 = np.linspace(0.0, 2*np.pi, 51)

# inciso c
x2 = np.arange(np.pi/2.0, 3*np.pi+np.pi/10, np.pi/10.)

# inciso d

y1 = f(x1)
y2 = f(x2)


# inciso e
import matplotlib.pyplot as plt

plt.plot(x1, y1, color='green', marker='o')
plt.plot(x2, y2, c='r', marker='>')
plt.xlabel('x')
plt.ylabel('eje y')

plt.show()
