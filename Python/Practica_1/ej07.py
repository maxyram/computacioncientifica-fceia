import numpy as np
import random

def ordenar(arr):
    for i in range(len(arr)-1):
        for j in range(i+1, len(arr)):
            if (arr[j, 0] < arr[i, 0]):
                #arr[[i, j]] = arr[[j, i]]
                aux = np.copy(arr[i,:])  # arr[i,:].copy()
                arr[i,:] = arr[j,:]
                arr[j,:] = aux
    return arr



x = np.empty((10, 2))

for i in range(len(x)):
    x[i, 0] = random.randrange(-5, 20)
    x[i, 1] = x[i,0]**2


#print(x)
x_ord = np.sort(x)

print('arreglo desordenado:\n {}'.format(x))
print(x[[0, 3]])

x_ord = ordenar(x)


print('arreglo ordenado: \n{}'.format(x_ord))
