import numpy as np
from sys import exit

def leer_matriz(filename):
    ''' Funcion para leer una matriz desde un archivo
    que en la primera linea tiene el numero de filas y columnas
    y luego en cada renglon tiene una fila de la matriz'''
    archivo = open(filename, 'r')

    texto = archivo.readlines()
    archivo.close()

    lista = []
    for i in range(len(texto)):
        if i==0: 
            pass
        else:
            linea = texto[i].split()
            for j,elemento in enumerate(linea):
                linea[j] = int(elemento)
            lista.append(linea)

    matriz = np.array(lista)
    return matriz

matrizA = leer_matriz('matriz_A.dat')
matrizB = leer_matriz('matriz_B.dat')
matrizC = leer_matriz('matriz_C.dat')

print(matrizA)

#matrizB = np.loadtxt('matriz_B.dat', skiprows=1)

#print(matrizB)


