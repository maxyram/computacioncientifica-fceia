import numpy as np
from sys import exit

def mat_prod(mA, mB):
    if ( mA.shape[1] != mB.shape[0]):
        print('Las matrices no se pueden multiplicar')
        print('La matriz A tiene {} filas'.format(mA.shape[1]))
        print('La matriz B tiene {} filas'.format(mB.shape[0]))
        exit()
    else :
        C = np.empty((mA.shape[0], mB.shape[1]))
        for i in range(mA.shape[0]):
            for j in range(mB.shape[1]):
                C[i,j] = np.dot(mA[i,:],mB[:,j])
    return C

A = np.loadtxt('matriz_A.dat', skiprows=1)
B = np.loadtxt('matriz_B.dat', skiprows=1)
C = np.loadtxt('matriz_C.dat', skiprows=1)


print(mat_prod(A, B))

print(np.matmul(A, B))