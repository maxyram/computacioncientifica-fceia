import random
import numpy as np

r = random.randrange(25, 60)
r_list_1 = [ random.randrange(0, 3) for i in range(r)]
r = random.randrange(25, 60)
r_list_2 = [ random.randrange(5, 7) for i in range(r)]
r = random.randrange(25, 60)
r_list_3 = [ random.randrange(9, 16) for i in range(r)]

r_list = r_list_1 + r_list_2 + r_list_3

random.shuffle(r_list)

arr = np.array([r_list])

np.savetxt('ej11.dat', arr, fmt="%2d", delimiter='\n')