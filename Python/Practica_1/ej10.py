import numpy as np
import timeit
from sys import exit

def mat_prod(mA, mB):
    if ( mA.shape[1] != mB.shape[0]):
        print('Las matrices no se pueden multiplicar')
        print('La matriz A tiene {} filas'.format(mA.shape[1]))
        print('La matriz B tiene {} filas'.format(mB.shape[0]))
        exit()
    else :
        C = np.empty((mA.shape[0], mB.shape[1]))
        for i in range(mA.shape[0]):
            for j in range(mB.shape[1]):
                C[i,j] = np.dot(mA[i,:],mB[:,j])
    return C

for n in [10, 50, 100, 500, 1000]:

    A = np.random.rand(n, n)
    B = np.random.rand(n, n)
    
    start_time = timeit.default_timer()
    C = mat_prod(A, B)
    end_time = timeit.default_timer()
    print('Matrices {}x{}'.format(n,n))
    print ('Tiempo algoritmo propio {} s'. format(end_time - start_time ))
    
    start_time = timeit.default_timer()
    C = np.matmul(A, B)
    end_time = timeit.default_timer()
    print ('Tiempo algoritmo NumPy {} s\n '. format(end_time - start_time ))

