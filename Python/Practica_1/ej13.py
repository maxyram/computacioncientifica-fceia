from ej12 import *
import numpy as np


def primes_decomp(n, f=2, factors=[]):
    '''Esta funcion halla la descomposicion en factores primos de un numero n.
       los argumentos f y factors no son necesarios para llamarla, estan solo 
       para poder devolver el resultado de la ejecucion recursiva.'''
    if n>=f :
        if n%f==0:
            factors.append(f)
            #print(f)
            primes_decomp(n//f, f, factors=factors)
        else:
            primes_decomp(n,next_prime(f), factors=factors)
    return(factors)
        

n=564
print('Se pretende hallar la descompocision en factores primos de \nn={}'.format(n))
decomp = primes_decomp(n)

print(*decomp, sep='*', end='={}\n'.format(np.prod(decomp)))
    
    