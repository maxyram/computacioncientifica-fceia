def notas_parcial(nombres, notas):
    if len(nombres) != len(notas):
        print('Las longitudes de las listas no coinciden')
    else :
        
        for persona, nota in zip(nombres, notas):
            str = '{} tu nota es {}.'.format(persona, nota)
            if nota >= 6:
                str += ' Felicitaciones aprobaste!'
            elif nota < 6:
                str += ' Vamos que en el recuperatorio sale!'
            print(str)
    
    

alumnos = ['Marie', 'Albert', 'Max']
calificaciones = [10, 4, 6]
notas_parcial(alumnos, calificaciones)