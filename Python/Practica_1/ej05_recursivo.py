# Ese codigo no funciona por el momento.
# Es un intento de hacer el problema de forma recursiva
# pero por el momento esta en desarrollo

def propagar(fosforos):    
    unos = [i for i,j in enumerate(fosforos) if j==1]
    for i in unos:
        encender_vecinos(i, fosforos)
    return fosforos


def encender_vecinos(i, fosforos):
    if fosforos[i-1]==0: 
        fosforos[i-1]=1
        encender_vecinos((i-1), fosforos)
    if fosforos[i+1]==0: 
        fosforos[i+1]=1
        encender_vecinos((i+1), fosforos)
    else: 
        pass

        
fosforos = [0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 1 ,0, 0, 0]

print('Inicio: {}'.format(fosforos))
fosforos = propagar(fosforos)
print('Final: {}'.format(fosforos))
