import numpy as np

def is_prime(n):
    '''Esta funcion chequea si un numero n es primo
    Devuelve: True  si n es primo
              False si n no es primo
    '''
    if not isinstance(n, int):
        print('Error: se debe ingresar un numero natural')
    if ((n==1) or (n%2 ==0 and n!=2)):
        return False
    for i in range(3, n//2, 2):
        if (n%i == 0):
            return False
    return True


def next_prime(n=1):
    '''Esta funcion devuelve el siguiente numero primo a un dado n.
       n no necesariamente debe ser primo'''
    if n<2:
        return 2
    elif n==2:
        return 3
    elif n%2==0:
        n -= 1   # Para que n sea impar
    while True:
        n += 2
        if is_prime(n): 
            return n


def primes_list(n, arr=False):
    '''Esta funcion genera una lista o arreglo con todos los numeros primos menores que n.
    Si arr=True se devuelve un numpy.array.
    Si arr=False se devuelve una lista
    (Default: arr=False)'''
    primes = [2]

    while next_prime(primes[-1])<n:
        primes.append(next_prime(primes[-1]))
    if arr:
        primes = np.array(primes)
    return primes

print(primes_list(34))